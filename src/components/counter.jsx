import React, { Component } from "react";

class Counter extends Component {
  render() {
    return (
      <div>
        <button
          onClick={() => this.props.onIncrement(this.props.counter)}
          className={this.buttonClassName()}
        >
          Increment
        </button>
        <span className="count">{this.countFormat()}</span>
        <button
          onClick={() => this.props.onDecrement(this.props.counter)}
          className={this.buttonClassName()}
        >
          Decrement
        </button>
        <button
          className="deleteButton"
          onClick={() => this.props.deleteFunc(this.props.counter.id)}
        >
          Delete
        </button>
      </div>
    );
  }

  buttonClassName=()=>{
    if(this.props.counter.value==0){
      return "button buttonColor1"
    }else{
      return "button buttonColor2"
    }
    
  }
  countFormat() {
    let count = this.props.counter.value;
    if (count === 0) {
      return "Zero";
    } else {
      return count;
    }
  }
}

export default Counter;
