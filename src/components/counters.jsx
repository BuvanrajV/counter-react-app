import React, { Component } from "react";
import Counter from "./counter";

class Counters extends Component {
  state = {
    counters: [],
    id: 0,
    resetClassName: "resetButton",
  };
  render() {
    return (
      <div className="container">
        <div>
          <button className="addButton" onClick={this.addCounter}>
            Add Counter
          </button>
          <button className="resetButton" onClick={this.resetCounters}>
            Reset
          </button>
        </div>
        {this.state.counters.map((counter) => (
          <Counter
            key={counter.id}
            deleteFunc={this.deleteCounter}
            onIncrement={this.increaseCount}
            onDecrement={this.decreaseCount}
            counter={counter}
          />
        ))}
      </div>
    );
  }

  addCounter = () => {
    let id = this.state.id + 1;
    let counters = [...this.state.counters];
    counters.push({ id: this.state.id, value: 0 });
    this.setState({ id, counters });
  };

  increaseCount = (counter) => {
    let counters = [...this.state.counters];
    let index = counters.indexOf(counter);
    counters[index].value++;
    this.setState({ counters });
  };

  decreaseCount = (counter) => {
    if (counter.value !== 0) {
      let counters = [...this.state.counters];
      let index = counters.indexOf(counter);
      counters[index].value--;
      this.setState({ counters });
    }
  };
  resetCounters = () => {
    let counters = this.state.counters.map((counter) => {
      counter.value = 0;
      return counter;
    });
    this.setState({ counters});
  };

  deleteCounter = (counterId) => {
    let counters = this.state.counters.filter(
      (counter) => counter.id !== counterId
    );
    this.setState({ counters});
  };
}

export default Counters;
