import React, { StrictMode } from "react";
import ReactDom from "react-dom/client";
import Header from "./components/header";
import Counters from "./components/counters";
import "./style.css";

const root = ReactDom.createRoot(document.getElementById("root"));

root.render(
  <StrictMode>
    <Header></Header>
    <Counters></Counters>
  </StrictMode>
);
